# httprust

Tutorial on how to use the canister http feature with `dfx`. An updated version of `dfx` is required.

### SDK

```bash
git clone https://github.com/dfinity/sdk
cd sdk
nix-env -iA nixpkgs.niv
# Update sdk replica and adapter version to newer version (this is from 7.7.2022)
bash scripts/update-replica.sh 9aecf11f5629999550901294a324c9b011492af2
# Update the ledger for e2e tests? (y/n) n
cargo build --release
# dfx binary is not in ./target/release/dfx
# Create alias to distinguish between regular dfx
alias dfx-http=${PWD}/target/release/dfx
```

### This Repo

```bash
git clone git@gitlab.com:tim.gretler/canisterhttpdfx.git
cd canisterhttpdfx
# Start replica and adapter in background
dfx-http start --enable-canister-http --host 127.0.0.1:8888 --background
# Create canister
dfx-http canister --network http://127.0.0.1:8888 create httprust
# Deploy canister (Don't forget 'rustup target add wasm32-unknown-unknown')
dfx-http deploy --network http://127.0.0.1:8888
```

### Interact with canister http feature

```bash
# Add cycles to all canisters
dfx-http ledger --network http://127.0.0.1:8888 fabricate-cycles --t 10 --all
# Make outgoing request.
dfx-http canister --network http://127.0.0.1:8888 call httprust send_request '(record {url="https://example.com";headers=vec {};body="";transform=""})'
# Check response of outgoing request
dfx-http canister  --network http://127.0.0.1:8888 call httprust check_response https://example.com
```