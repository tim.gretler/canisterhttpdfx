//! Defines a canister which is used in testing Canister HTTP Calls feature.
//!
//! The canister receives HTTP request through inbound message, decodes the HTTP request
//! and forwards it to targeted service. Canister returns the remote service call response
//! as a canister message to client if the call was successful and agreed by majority nodes,
//! otherwise errors out.
//!

mod ic00;
use candid::CandidType;
use candid::{candid_method, Principal};
use ic00::{CanisterHttpRequestArgs, CanisterHttpResponsePayload, HttpHeader, HttpMethod};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::collections::HashMap;

#[derive(CandidType, Serialize, Deserialize, Debug, Clone)]
pub struct RemoteHttpRequest {
    pub url: String,
    pub headers: Vec<(String, String)>,
    pub body: String,
    pub transform: String,
}

#[derive(CandidType, Serialize, Deserialize, Debug, Clone)]
pub struct RemoteHttpResponse {
    pub status: u8,
    pub headers: Vec<(String, String)>,
    pub body: String,
}

impl RemoteHttpResponse {
    pub fn new(status: u8, headers: Vec<(String, String)>, body: String) -> Self {
        Self {
            status,
            headers,
            body,
        }
    }
}

thread_local! {
    pub static REMOTE_CALLS: RefCell<HashMap<String,RemoteHttpResponse>>  = RefCell::new(HashMap::new());
}

#[ic_cdk_macros::update(name = "send_request")]
#[candid_method(update, rename = "send_request")]
async fn send_request(request: RemoteHttpRequest) -> Result<(), String> {
    let canister_http_headers = request
        .headers
        .into_iter()
        .map(|header| HttpHeader {
            name: header.0,
            value: header.1,
        })
        .collect();
    ic_cdk::println!("send_request being called");

    let canister_http_request = CanisterHttpRequestArgs {
        url: request.url.clone(),
        http_method: HttpMethod::GET,
        body: Some(request.body.as_bytes().to_vec()),
        transform_method_name: None,
        headers: canister_http_headers,
        max_response_bytes: Some(1000000),
    };

    ic_cdk::println!("send_request encoding CanisterHttpRequestArgs message.");
    let encoded_req = candid::utils::encode_one(&canister_http_request).unwrap();

    ic_cdk::println!("send_request making IC call.");
    match ic_cdk::api::call::call_raw(
        Principal::management_canister(),
        "http_request",
        &encoded_req[..],
        1000000000000,
    )
    .await
    {
        Ok(raw_response) => {
            println!("send_request returning with success case.");
            let decoded: CanisterHttpResponsePayload = candid::utils::decode_one(&raw_response)
                .map_err(|err| return format!("Decoding raw http response failed: {}", err))?;
            let mut response_headers = vec![];
            for header in decoded.headers {
                response_headers.push((header.name, header.value));
            }
            let response = RemoteHttpResponse::new(
                decoded.status as u8,
                response_headers,
                String::from_utf8(decoded.body).map_err(|err| return format!("Internet Computer cansiter HTTP calls expects request and response be UTF-8 encoded. However, the response content is not UTF-8 compliant. Error: {}", err))?,
            );

            let request_url = request.url.clone();
            REMOTE_CALLS.with(|results| {
                let mut writer = results.borrow_mut();
                writer.entry(request_url).or_insert(response);
            });
            Result::Ok(())
        }
        Err((r, m)) => {
            ic_cdk::println!("send_request returning with failure case.");
            let error = format!(
                "Failed to send request to {}. CanisterId: {}, RejectionCode: {:?}, Message: {}",
                &request.url,
                ic_cdk::id(),
                r,
                m
            );
            Err(error)
        }
    }
}

#[ic_cdk_macros::query(name = "check_response")]
#[candid_method(query, rename = "check_response")]
async fn check_response(url: String) -> Result<RemoteHttpResponse, String> {
    println!("check_response being called");
    REMOTE_CALLS.with(|results| {
        let reader = results.borrow();
        println!("Size of dictionary is: {}", reader.len());
        match reader.get(&url) {
            Some(x) => {
                println!("check_response returning with success case.");
                Result::Ok(x.clone())
            }
            _ => {
                println!("check_response returning with failure case.");
                let message = format!("Request to URL {} has not been made.", url);
                println!("{}", message);
                Result::Err(message)
            }
        }
    })
}

#[ic_cdk_macros::query(name = "transform")]
#[candid_method(query, rename = "transform")]
fn transform(raw: CanisterHttpResponsePayload) -> CanisterHttpResponsePayload {
    let mut transformed = raw;
    transformed.headers = vec![];
    transformed
}
